package models

import (
	"fmt"
	"github.com/robfig/revel"
	"regexp"
)

type Post struct {
	PostId	 int
	AuthorId int
	Title	 string
	Body	 string
}

func (post *Post) Validate(v *revel.Validation) {
	v.Check(Post.Body,
		revel.Required{},
		revel.MinSize{1},
		revel.MaxSize{10000}
	)

	v.Check(Post.Title,
		revel.Required{},
		revel.MinSize{1},
		revel.MaxSize{80}
	)
}