package models

import (
	"fmt"
	"github.com/robfig/revel"
	"regexp"
)

type Comment struct {
	PostId	  int
	AuthorId  int
	CommentId int
	Body   	  string
}

func (comment *Comment) Validate(v *revel.Validation) {
	v.Check(comment.Body,
		revel.Required{},
		revel.MinSize{1},
		revel.MaxSize{140}
	)
}