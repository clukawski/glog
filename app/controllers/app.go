package controllers

import (
	"github.com/robfig/revel"
	"glog/app/models"
	"code.google.com/p/go.crypto/bcrypt"
)

const host = "localhost"

type App struct {
	GorpController
}

func (a App) AddUser() revel.Result {
	if user := a.connected(); user != nil {
		a.RenderArgs["user"] = user
	}
	return nil
}

func (a App) connected() *models.User {
	if a.RenderArgs["user"] != nil {
		return a.RenderArgs["user"].(*models.User)
	}
	if username, ok := a.Session["user"]; ok {
		return a.getUser(username)
	}
	return nil
}

func (a App) getUser(username string) *models.User {
	users, err := a.Txn.Select(models.User{}, `select * from User where Username = ?`, username)
	if err != nil {
		panic(err)
	}
	if len(users) == 0 {
		return nil
	}
	return users[0].(*models.User)
}

func (a App) Comment() revel.Result {
	if a.connected() != nil {
		return a.Redirect(routes.Hotels.Index())
	}
	a.Flash.Error("Please log in first")
	return a.Render()
}

func (a App) Register() revel.Result {
	return a.Render()
}

func (a App) SaveUser(user models.User, verifyPassword string) revel.Result {
	a.Validation.Required(verifyPassword)
	a.Validation.Required(verifyPassword == user.Password).
		Message("Password does not match")
	user.Validate(a.Validation)

	if a.Validation.HasErrors() {
		a.Validation.Keep()
		a.FlashParams()
		return a.Redirect(routes.Application.Register())
	}

	user.HashedPassword, _ = bcrypt.GenerateFromPassword(
		[]byte(user.Password), bcrypt.DefaultCost)
	err := a.Txn.Insert(&user)
	if err != nil {
		panic(err)
	}

	a.Session["user"] = user.Username
	a.Flash.Success("Welcome, " + user.Name)
	return a.Redirect(routes.Hotels.Index())
}

func (a App) Login(username, password string) revel.Result {
	user := a.getUser(username)
	if user != nil {
		err := bcrypt.CompareHashAndPassword(user.HashedPassword, []byte(password))
		if err == nil {
			a.Session["user"] = username
			a.Flash.Success("Welcome, " + username)
			return a.Redirect(routes.Hotels.Index())
		}
	}

	a.Flash.Out["username"] = username
	a.Flash.Error("Login failed")
	return a.Redirect(routes.Application.Index())
}

func (a App) Logout() revel.Result {
	for k := range a.Session {
		delete(a.Session, k)
	}
	return a.Redirect(routes.Application.Index())
}
